//
//  ViewController.swift
//  Image Viewer
//
//  Created by Click Labs on 1/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var imageNum = 0
    
    @IBOutlet weak var imNum: UITextField!
    
    @IBOutlet weak var imageviewer: UIImageView!
    
    @IBOutlet weak var errorMessage: UILabel!
    
    @IBAction func searchButton(sender: AnyObject) {
        
        var str = imNum.text.toInt()        
        
        if str != nil {
            
            imageNum = str!
            
        }
        
        if imNum.text.toInt() != nil {

            displayImage(imageNum)
            
            
        }else {
            
        errorMessage.text = "Please enter a valid number"
            
        }
    }
    // function to display images
    func displayImage(photoNum: Int){
        
        switch(photoNum)
        {
        case 1 :
            
            var image = UIImage(named: "1.jpg")
            imageviewer.image = image
        case 2 :
            
            var image = UIImage(named: "2.jpg")
            imageviewer.image = image
        case 3 :
            
            var image = UIImage(named: "3.jpg")
            imageviewer.image = image
        case 4 :
            
            var image = UIImage(named: "4.jpg")
            imageviewer.image = image
        case 5 :
            
            var image = UIImage(named: "5.jpg")
            imageviewer.image = image
        case 6 :
            
            var image = UIImage(named: "6.jpg")
            imageviewer.image = image
        case 7 :
            
            var image = UIImage(named: "7.jpg")
            imageviewer.image = image
        case 8 :
            
            var image = UIImage(named: "8.jpg")
            imageviewer.image = image
        case 9 :
            
            var image = UIImage(named: "9.jpg")
            imageviewer.image = image
        case 10 :
            
            var image = UIImage(named: "10.jpg")
            imageviewer.image = image
            
        default :
            
            errorMessage.text = "Please enter value between 1-10 "
        }
    }
    
    // randomly display an image
    @IBAction func randomPic(sender: AnyObject) {
        
        imageNum = Int(arc4random_uniform(11))
        
        if imageNum != 0 || imageNum > 10 {
            
        displayImage(imageNum)
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

